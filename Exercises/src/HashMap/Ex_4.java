package HashMap;

import java.util.HashMap;

public class Ex_4 {
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(1, "Red");
        hashMap.put(2, "Green");
        hashMap.put(3, "Yellow");
        hashMap.put(4, "Black");
        hashMap.put(5, "White");
        hashMap.put(6,"Blue");

        System.out.println("HashMap: "+ hashMap);
        hashMap.clear();
        System.out.println("The new map: "+hashMap);
    }
}
