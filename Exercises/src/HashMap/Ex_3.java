package HashMap;

import java.util.HashMap;

public class Ex_3 {
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        HashMap<Integer, String> hashMap1 = new HashMap<>();
        hashMap.put(1, "Red");
        hashMap.put(2, "Green");
        hashMap.put(3, "Yellow");
        System.out.println("Value in first map: "+hashMap);

        hashMap.put(4, "Black");
        hashMap.put(5, "White");
        hashMap.put(6,"Blue");
        System.out.println("Value in second map: "+ hashMap1);

        hashMap1.putAll(hashMap);
        System.out.println("Now value in second map: "+ hashMap1);
    }
}
