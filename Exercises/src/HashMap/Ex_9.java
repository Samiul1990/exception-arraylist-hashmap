package HashMap;

import java.util.HashMap;
import java.util.Set;

public class Ex_9 {
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(1, "Red");
        hashMap.put(2, "Green");
        hashMap.put(3, "Yellow");
        hashMap.put(4, "Black");
        hashMap.put(5, "White");
        hashMap.put(6,"Blue");

        Set set = hashMap.entrySet();
        System.out.println("Set values: "+set);
    }
}
