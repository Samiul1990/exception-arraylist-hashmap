package HashMap;

import java.util.HashMap;

public class Ex_7 {
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(1, "Red");
        hashMap.put(2, "Green");
        hashMap.put(3, "Yellow");
        hashMap.put(4, "Black");
        hashMap.put(5, "White");
        hashMap.put(6,"Blue");
        System.out.println("Original map: "+ hashMap);

        System.out.println("1. is key 'Green' exists? ");
        System.out.println(hashMap.containsKey(2));
    }
}
