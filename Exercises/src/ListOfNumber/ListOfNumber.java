package ListOfNumber;

import java.io.*;
import java.util.List;
import java.util.ArrayList;

public class ListOfNumber {
    private List<Integer> list;
    private static final int SIZE = 10;

    public ListOfNumber() {
        list = new ArrayList<Integer>(SIZE);
        for (int i = 0; i < SIZE; i++)
            list.add(i);
    }

    public void writeList() {
        PrintWriter out = null;

        try {
            System.out.println("Entering try statement");
            out = new PrintWriter(new FileWriter("OutFile.txt"));

            for (int i = 0; i < SIZE; i++)
                out.println(list.get(i));
        } catch (IndexOutOfBoundsException e) {
            System.err.println("Caught IndexOutOfBoundsException: " +
                    e.getMessage());
        } catch (IOException e) {
            System.err.println("Caught IOException: " + e.getMessage());
        } finally {
            if (out != null) {
                System.out.println("Closing PrintWriter");
                out.close();
            } else {
                System.out.println("PrintWriter not open");
            }
        }
    }

    public void readList(){
        FileReader fr = null;
        int ch;
        try {
            fr = new FileReader("OutFile.txt");
            BufferedReader br = new BufferedReader(fr);
            while ((br.readLine()) != null) {
                ch = Integer.parseInt(br.readLine());
                System.out.println(ch);
                list.add(ch);
            }
            System.out.println(list);
        } catch (FileNotFoundException fe) {
            System.out.println("Caught FileNotFoundException: " + fe.getMessage());
        } catch (IOException ie) {
            System.out.println("Caught IOException: " + ie.getMessage());
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Caught IndexOutOfBoundsException: " + e.getMessage());
        }
        finally {
            if (fr != null) {
                try{
                    fr.close();
                }
                catch (IOException ie){
                    System.out.println("Caught IOException: "+ie.getMessage());
                }
            } else {
                System.out.println("File not opened");
            }

        }
    }
}
