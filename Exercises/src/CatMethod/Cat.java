package CatMethod;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Cat {

    public class CatMain {
        public static void main(String[] args) {

        }
        public static void cat(File named){
            RandomAccessFile input = null;
            String line = null;

            try {
                input = new RandomAccessFile(named, "r");
                while ((line = input.readLine()) != null) {
                    System.out.println(line);
                }
                return;
            }
            catch (FileNotFoundException fe){
                System.out.println("Caught FileNotFoundException: "+fe.getMessage());
            }
            catch (IOException ie){
                System.out.println("Caught IOException: "+ie.getMessage());
            }
            finally {
                if (input != null) {
                    try{
                        input.close();
                    }
                    catch (IOException ie){
                        System.out.println("Caught IOException: "+ie.getMessage());
                    }
                }
            }
        }
    }
}
