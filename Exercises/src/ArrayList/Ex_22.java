package ArrayList;

import java.util.ArrayList;

public class Ex_22 {
    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Yellow");
        colors.add("Black");
        colors.add("Orange");
        System.out.println("ArrayList: "+colors);

        System.out.println("\n Print using index of an elements: ");
        int noOfElements = colors.size();
        for (int i=0; i<noOfElements; i++)
        {
            System.out.println(colors.get(i));
        }
    }
}
