package ArrayList;

import java.util.ArrayList;

public class Ex_15 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("A");
        list.add("B");
        list.add("C");
        list.add("D");
        list.add("E");
        System.out.println("List: "+ list);

        ArrayList<String> list1 = new ArrayList<>();
        list1.add("1");
        list1.add("2");
        list1.add("3");
        list1.add("4");
        list1.add("5");
        System.out.println("List1: "+list1);

        ArrayList<String> join = new ArrayList<>();
        join.addAll(list);
        join.addAll(list1);

        System.out.println("After join two ArrayList: "+join);
    }
}
