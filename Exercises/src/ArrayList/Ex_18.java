package ArrayList;

import java.util.ArrayList;

public class Ex_18 {
    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList<String>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Yellow");
        colors.add("Black");
        colors.add("Orange");

        System.out.println("ArrayList: "+colors);
        System.out.println("Checking the ArrayList is Empty or not: "+ colors.isEmpty());

        colors.removeAll(colors);
        System.out.println("Checking the ArrayList is Empty or not: "+ colors.isEmpty());
    }
}
