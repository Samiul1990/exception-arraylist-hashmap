package ArrayList;

import java.util.ArrayList;

public class ArrayList_Ex_1 {
    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Yellow");
        colors.add("Black");
        colors.add("Orange");

        System.out.println("Add some color in ArrayLis: "+"\n" +colors);
    }
}
