package ArrayList;

import java.util.ArrayList;

public class ArrayList_Ex_4 {
    public static void main(String[] args) {
        String element ="";
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Yellow");
        colors.add("Black");
        colors.add("Orange");

        System.out.println(colors);

        element = colors.get(0);
        System.out.println("First Element: " + element);

        element = colors.get(3);
        System.out.println("Third Element: " + element);
    }
}
