package ArrayList;

import java.util.ArrayList;

public class ArrayList_Ex_6 {
    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Yellow");
        colors.add("Black");
        colors.add("Orange");

        System.out.println(colors);

        colors.remove(2);
        System.out.println("After removing third element from the list:\n" + colors);
    }
}
