package ArrayList;

import java.util.ArrayList;
import java.util.Collections;

public class Ex_11 {
    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Yellow");
        colors.add("Black");
        colors.add("Orange");
        System.out.println("ArrayList before reverse elements: "+"\n" +colors+"\n");

        Collections.reverse(colors);
        System.out.println("ArrayList after reverse elements: "+"\n" +colors);
    }
}
