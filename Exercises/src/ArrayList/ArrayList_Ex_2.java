package ArrayList;

import java.util.ArrayList;

public class ArrayList_Ex_2 {
    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Yellow");
        colors.add("Black");
        colors.add("Orange");

        for (String elements :colors ){
            System.out.println(elements);
        }
    }
}
