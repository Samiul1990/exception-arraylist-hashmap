package ArrayList;

import java.util.ArrayList;

public class Ex_16 {
    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList<String>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Yellow");
        colors.add("Black");
        colors.add("Orange");
        System.out.println("Original ArrayList: "+colors);
        ArrayList<String> color1 = (ArrayList<String>)colors.clone();
        System.out.println("After cloned ArrayList: " +color1);
    }
}
