package ArrayList;

import java.util.ArrayList;
import java.util.List;

public class Ex_12 {
    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList<String>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Yellow");
        colors.add("Black");
        colors.add("Orange");
        System.out.println("Colors: "+colors);
        List<String> subColor = colors.subList(0, 3);
        System.out.println("ArrayList of first three elements: "+subColor);
    }
}
