package ArrayList;

import java.util.ArrayList;

public class Ex_13 {
    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Yellow");
        colors.add("Black");
        colors.add("Orange");
        System.out.println("Colors: "+colors);

        ArrayList<String> colors1 = new ArrayList<>();
        colors1.add("Red");
        colors1.add("Green");
        //colors1.add("Yellow");
        colors1.add("Black");
        colors1.add("Orange");
        System.out.println("Colors: "+colors1);

        ArrayList<String> colors2 = new ArrayList<>();
        for (String s : colors)
        {
            colors2.add(colors1.contains(s) ? "Yes" : "No");
            System.out.println(colors2);
        }
    }
}
