package ArrayList;

import java.util.ArrayList;

public class Ex_19 {
    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Yellow");
        colors.add("Black");
        colors.add("Orange");
        System.out.println("ArrayList: "+colors);

        colors.trimToSize();
        System.out.println(colors);
    }
}
