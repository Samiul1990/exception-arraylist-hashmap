package ArrayList;

import java.util.ArrayList;

public class Ex_17 {
    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList<String>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Yellow");
        colors.add("Black");
        colors.add("Orange");
        System.out.println("ArrayList: "+colors);

        colors.removeAll(colors);
        System.out.println("After removed all all elements of ArrayList: "+ colors);
    }
}
