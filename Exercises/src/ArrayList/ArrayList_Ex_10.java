package ArrayList;

import java.util.ArrayList;
import java.util.Collections;

public class ArrayList_Ex_10 {
    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Yellow");
        colors.add("Black");
        colors.add("Orange");
        System.out.println("ArrayList before shuffling:\n"+colors);

        Collections.shuffle(colors);
        System.out.println("ArrayList after shuffling:\n"+colors);
    }
}
