package ArrayList;

import java.util.ArrayList;
import java.util.Collections;

public class ArrayList_Ex_9 {
    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Yellow");
        colors.add("Black");
        colors.add("Orange");
        System.out.println("Color: "+colors);

        ArrayList<String> list= new ArrayList<>();
        list.add("R");
        list.add("G");
        list.add("Y");
        list.add("B");
        list.add("O");
        System.out.println("List: "+list);

        Collections.copy(colors, list);
        System.out.println("ArrayList color copy to list: ");
        System.out.println("Color: "+colors);
        System.out.println("List: "+list);

    }
}
