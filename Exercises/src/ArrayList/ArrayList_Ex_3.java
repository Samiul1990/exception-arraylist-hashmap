package ArrayList;

import java.util.ArrayList;

public class ArrayList_Ex_3 {
    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Yellow");
        colors.add("Black");
        colors.add("Orange");

        System.out.println(colors);

        colors.add(0,"Blue");
        System.out.println("After add element of array list at the first and six position"+"\n"+colors);
        colors.add(6,"Gray");
        System.out.println(colors);
    }
}
