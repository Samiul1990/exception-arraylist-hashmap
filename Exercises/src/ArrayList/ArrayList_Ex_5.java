package ArrayList;

import java.util.ArrayList;

public class ArrayList_Ex_5 {
    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Yellow");
        colors.add("Black");
        colors.add("Orange");

        System.out.println(colors);

        colors.set(3,"White");
        System.out.println("After update specific array element: "+"\n"+colors);
        colors.set(4,"Tomato");
        System.out.println(colors);
    }
}
