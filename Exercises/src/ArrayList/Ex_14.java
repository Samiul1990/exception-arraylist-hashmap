package ArrayList;

import java.util.ArrayList;
import java.util.Collections;

public class Ex_14 {
    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList<String>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Yellow");
        colors.add("Black");
        colors.add("Orange");
        System.out.println("Colors: "+colors);

        Collections.swap(colors, 0, 2);
        System.out.println("After swap ArrayList: "+colors);
    }
}
