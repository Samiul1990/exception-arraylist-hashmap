package ArrayList;

import java.util.ArrayList;
import java.util.Collections;

public class Ex_21 {
    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Yellow");

        System.out.println("Original ArrayList: "+colors);
        String newColor = "Blue";
        colors.set(2, newColor);

        int num = colors.size();
        System.out.println("After replace third element with 'Blue'. ");
        for (int i = 0; i < num; i++)
        {
            System.out.println(colors.get(i));
        }

    }
}
